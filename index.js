let http = require("http");

const port = 8000;

http.createServer(function(request, response){
	if(request.url == '/login'){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('You are in the login page')
	}
	else if(request.url == '/register'){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('You are in the registration page')
	}
	else if(request.url == '/homepage'){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('You are in the home page')
	}
	else if(request.url == '/usersProfile'){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('You are in the users profile page')
	}
	else{
		response.writeHead(404, {'Conten-Type' : 'text/plain'})
		response.end('Page not found!')
	}
}).listen(port)

console.log(`Server is successfully running at localhost: ${port}`);