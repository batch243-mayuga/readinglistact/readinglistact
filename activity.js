// 1) price equal or greater than 20

db.fruits.find({price: {$gte: 20}}).pretty();

// 2) stocks greater than 10 and less than 30

db.fruits.find({stock: {$gt: 10, $lt: 30}});

// 3) find fruit that has 'a' in their name and should be from the supplier_id "1"

db.fruits.find({$and: [{supplier_id: 1},{name: {$regex: 'a'}}]});

// 4) update the supplier _id to 1 if the name of the fruits has 'n'

db.fruits.updateMany({name: {$regex: 'n'}},{
	$set: {
		supplier_id: 1
	}
});

// 5) update the price of several fruits that has a stocks less than or equal to 15. New price should be 200

db.fruits.updateMany({stock: {$lte: 15}},{
	$set: {
		price: 200
	}
});